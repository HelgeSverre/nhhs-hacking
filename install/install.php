<?php
session_start();

$hostname = $_POST["host"];
$database = $_POST["database"];
$username = $_POST["username"];
$password = $_POST["password"];

try {

    $db = new PDO("mysql:host=$hostname;dbname=$database", $username, $password);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec(file_get_contents("./import.sql"));


    $config = [
        "hostname" => $hostname,
        "database" => $database,
        "username" => $username,
        "password" => $password
    ];

    // Simply build the config.php file as a string
    $configString = "<?php \n\nreturn " . var_export($config, true) . ";";

    // Store config file
    file_put_contents(__DIR__ . "/../config.php", $configString);

} catch (PDOException $exception) {
    $_SESSION["error"] = $exception->getMessage();
    header("Location: /install");
    die;
}

// If we've come this far, we've successfully installed.
?>

    <h1>Installation succesfully</h1>
    <p>Redirecting in 2 seconds, please wait...</p>

    <script>
        setTimeout(function () {
            window.location = "/";
        }, 2000)
    </script>

<?php
