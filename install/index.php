<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>NHHS Hacking Workshop</title>

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FONT -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css">

    <!-- CSS -->
    <link rel="stylesheet" href="/assets/css/normalize.css">
    <link rel="stylesheet" href="/assets/css/skeleton.css">
    <link rel="stylesheet" href="/assets/css/custom.css">

</head>
<body>

<div class="wrapper">

    <div class="container" >
        <div class="row">
            <div class="three columns">&nbsp;</div>
            <div class="six columns">
                <div class="box">
                    <h4>Installasjon av demo-applikasjon</h4>
                    <p>Denne siden vil opprette en konfigurasjonsfil for database credentials
                        samt vil kjøre en SQL spørring for å opprette og fylle tabeller som trengs.</p>

                    <?php if (isset($_SESSION["error"])): ?>
                        <div class="error">
                            <?= $_SESSION["error"] ?>
                        </div>
                        <?php unset($_SESSION["error"]); // Clear error after showing it. ?>
                    <?php endif; ?>

                    <form method="post" action="/install/install.php">
                        <label for="host">Host</label>
                        <input type="text" name="host" id="host" class="u-full-width" value="localhost">

                        <label for="database">Database</label>
                        <input type="text" name="database" id="database" class="u-full-width" value="hacking_workshop">

                        <label for="username">Brukernavn</label>
                        <input type="text" name="username" id="username" class="u-full-width" value="root">

                        <label for="password">Passord</label>
                        <input type="text" name="password" id="password" class="u-full-width">


                        <button class="button u-pull-right">Install</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
