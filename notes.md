## Cheat Sheet / Solutions

Because i will forget most of these when on-stage, i wrote them down in this file to copy-paste for later.

## SQL Injection Example #1

Ignore the rest of the query, return ALL records in a LIKE query:

```
 select * from customers where name like 'SEARCH_TERM%' and is_secret = 0 
```

Injection:

```
%' -- or name like '
``` 


## XSS Example

Put følgende i søkefelt for dramatisk kattebilde 

```
<img src="https://i.ytimg.com/vi/2fb-g_V-UT4/hqdefault.jpg">
```



## CSRF example

```
<img src="http://hacking.local/pages/csrf.php?transfer_to=3&amount=500">
```
### Protection

On login generere en token i sessionen, sjekk denne on-transfer

```
$_SESSION["token"] = md5(rand());
```



### Command Injection

http://php.net/manual/en/function.escapeshellcmd.php

```
escapeshellcmd($command)
```