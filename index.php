<?php include "./inc/header.php" ?>

    <div class="box" style="margin-bottom: 20px">
        <h4>Workshop om sikkerhet i web applikasjoner</h4>

        <p>Målet med denne nettsiden og presentasjonen er å vise webutviklere noen vanlige feil og mangler
            som kan fører til at nettsider og applikasjoner blir sårbare for angrep som
            kan føre til nedetid og tap av data.</p>

        <p>
            <strong>Git repository (BitBucket) </strong>

            <span style="margin: 20px 0; display: block;">
                <code>git clone https://bitbucket.org/HelgeSverre/nhhs-hacking</code>
            </span>

            <a href="https://bitbucket.org/HelgeSverre/nhhs-hacking" class="button button-primary">
                Gå til repo
            </a>
        </p>

        <h4>Installasjon av database</h4>

        <p>Om du har lastet ned denne applikasjonen og vil installere databasen på din egen maskin, klikk på knappen
            nedenfor for å gå til installasjonsveiviser.</p>


        <a href="/install" class="button button-primary">
            Installasjon
        </a>
    </div>

<?php include "./inc/footer.php" ?>