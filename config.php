<?php

// This file is the global configuration for the entire application.
return [
    "hostname" => "localhost",
    "database" => "hacking_workshop",
    "username" => "root",
    "password" => null
];