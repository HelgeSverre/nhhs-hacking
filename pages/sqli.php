<?php include "../inc/header.php" ?>

<?php

// Connect to DB
$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
$searchTerm = $_GET["search"] ?? "";
$customers = $db->query("select * from customers where name like '{$searchTerm}%' and is_secret = 0 LIMIT 10")->fetchAll();

?>


<div class="blue-bar">Eksempel på SQL Injection</div>
<div class="box" style="margin-bottom: 30px;">

    <form method="get">
        <input type="text" name="search" placeholder="Search customers..." value="<?= $searchTerm ?>">
        <button type="submit" class="button">Search</button>
    </form>

    <table class="u-full-width">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Country</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($customers as $customer): ?>
            <tr>
                <td><?= $customer["name"] ?></td>
                <td><?= $customer["email"] ?></td>
                <td><?= $customer["country"] ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>


<?php include "../inc/footer.php" ?>

