<?php include "../inc/header.php" ?>

<?php

$host = "";
$output = "";
$command = "";

if (isset($_GET["host"])) {
    $host = $_GET["host"];
    $command = "whois $host";
    $output = shell_exec($command);
}

?>

<div class="blue-bar">Eksempel på Command Injection</div>
<div class="box">

    <form method="GET">
        <div class="row">

            <div class="nine columns">
                <label for="host">Perform WHOIS lookup</label>
                <input type="text" name="host" id="host" class="u-full-width" placeholder="helgesverre.no"
                       value="<?= $host ?>">
            </div>
            <div class="three columns">
                <label>&nbsp;</label>
                <button type="submit" class="button pull">Ping host</button>
            </div>
        </div>
    </form>

    <hr>


    <div>
        <strong>Command:</strong>
        <code><?= $command ?></code>
    </div>

    <hr>

    <pre><?= $output ?></pre>


</div>


<?php include "../inc/footer.php" ?>

