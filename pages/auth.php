<?php include "../inc/header.php" ?>

<?php

if (isset($_GET["logout"])) {
    session_destroy();
    header("Location: /pages/auth.php");
    die;
}

// Run this code when POSTing the form
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["username"];
    $password = $_POST["password"];

    $stmt = $db->query("SELECT * FROM `users` WHERE username = '$username' AND password = '$password'");
    $userExists = $stmt->rowCount();

    if ($userExists) {
        $_SESSION["username"] = $username;
    }
}


$loggedIn = true;

if (!isset($_SESSION["username"])) {
    $loggedIn = false;
}


?>

<div class="blue-bar">Eksempel på Broken Authentication</div>
<div class="box">
    <?php if (!$loggedIn): ?>

        <form method="POST">

            <div class="row">
                <div class="one-half column">
                    <label for="username">Username</label>
                    <input class="u-full-width" type="text" name="username" id="username" placeholder="Username">

                </div>
                <div class="one-half column">
                    <label for="password">Password</label>
                    <input class="u-full-width" type="password" name="password" id="password" placeholder="Password">
                </div>
            </div>

            <button type="submit" class="button u-pull-right">Login</button>
        </form>

    <?php else: ?>

        <p>Welcome to the <strong style="color:red;">most secure page</strong> on the interweebz</p>

        <a href="/pages/auth.php?logout" class="button">Logg Ut</a>

    <?php endif; ?>

</div>


<?php include "../inc/footer.php" ?>

