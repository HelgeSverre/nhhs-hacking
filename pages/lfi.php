<?php include "../inc/header.php" ?>

<?php

$images = array_diff(scandir("../data"), ['..', '.']);
$rows = array_chunk($images, 3);

?>

<div class="blue-bar">Eksempel på Local File Inclusion</div>
<div class="box">

    <?php foreach ($rows as $images): ?>
        <div class="row">
            <?php foreach ($images as $image): ?>
                <div class="four columns">
                    <img src="/pages/lfi-load-image.php?image=<?= $image ?>" style="width: 100%;">
                </div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
</div>


<?php include "../inc/footer.php" ?>

