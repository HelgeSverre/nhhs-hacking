<?php include "../inc/header.php" ?>

<?php


// We are creating a new comment
if (isset($_POST["name"]) && isset($_POST["body"])) {
    $stmt = $db->prepare("INSERT INTO comments (name, body) VALUES (:name, :body)");
    $stmt->execute([
        "name" => $_POST["name"],
        "body" => $_POST["body"],
    ]);

    header("Location: /pages/stored-xss.php");
    die;
}


// Deleting a comment
if (isset($_GET["delete"])) {
    $stmt = $db->prepare("DELETE FROM comments WHERE id = :id");
    $stmt->execute(["id" => $_GET["delete"]]);
}


$comments = $db->query("SELECT * FROM comments ORDER BY posted_on DESC")->fetchAll();

?>


<div class="blue-bar">Eksempel på Stored Cross Site Scripting (Stored XSS)</div>
<div class="box" style="margin-bottom: 30px;">

    <div class="u-cf u-full-width">
        <form method="post">
            <input type="text" class="u-full-width" name="name" placeholder="Your name">
            <textarea class="u-full-width" style="height: 120px;" name="body" placeholder="Comment"></textarea>
            <button type="submit" class="button u-pull-right">Post Comment</button>
        </form>
    </div>

    <h4>Comments (<?= count($comments); ?>)</h4>

    <div class="comments">

        <?php if ($comments): ?>
            <?php foreach ($comments as $comment): ?>
                <hr>
                <div class="comment u-full-width u-cf">
                    <strong><?= $comment["name"] ?></strong>
                    <span class="u-pull-right">Posted on <?= $comment["posted_on"] ?></span>
                    <p style="margin-top: 10px;"><?= $comment["body"] ?></p>

                    <a href="?delete=<?= $comment["id"]; ?>" class="u-pull-right">Delete</a>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <p>No comments have been posted</p>
        <?php endif; ?>

    </div>
</div>


<?php include "../inc/footer.php" ?>

