<?php include "../inc/header.php" ?>
<?php


// Logout
if (isset($_GET["logout"])) {
    session_destroy();
    header("Location: /pages/csrf.php");
    die;
}

// Login
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $stmt = $db->prepare("SELECT * FROM `bank_users` WHERE username = :username AND password = :password");
    $stmt->execute([
        ":username" => $_POST["username"],
        ":password" => $_POST["password"]
    ]);

    $user = $stmt->fetch();

    if ($user) {
        $_SESSION["user"] = $user;
    } else {
        $_SESSION["error"] = "Wrong username or password";
    }
}


$loggedIn = false;

if (isset($_SESSION["user"])) {

    // Update our user info
    $stmt = $db->prepare("SELECT * FROM `bank_users` WHERE id = :id");
    $stmt->execute([":id" => $_SESSION["user"]["id"]]);

    $_SESSION["user"] = $stmt->fetch();
    $loggedIn = true;
}


if ($loggedIn) {

    // Fetch all bank customers, except our own user.
    $stmt = $db->prepare("SELECT * FROM bank_users WHERE id != :userId");
    $stmt->execute([":userId" => $_SESSION["user"]["id"]]);
    $bankUsers = $stmt->fetchAll();


    // Transfer money to other user
    if (isset($_GET["transfer_to"])) {
        $transferUser = $_GET["transfer_to"];
        $transferAmount = $_GET["amount"];

        // Decrement our own amount
        $stmt = $db->prepare("UPDATE bank_users SET amount = :newAmount WHERE id = :userId")->execute([
            ":newAmount" => $_SESSION["user"]["amount"] - $transferAmount,
            ":userId" => $_SESSION["user"]["id"],
        ]);


        // Increase receiving user's amount
        $stmt = $db->prepare("UPDATE bank_users SET amount = amount + :newAmount WHERE id = :userId")->execute([
            ":newAmount" => $transferAmount,
            ":userId" => $transferUser,
        ]);

        // Redirect to same page without query parameters
        header("Location: /pages/csrf.php");
        die;
    }
}

?>


<div class="blue-bar">Eksempel på Cross Site Request Forgery (CSRF)</div>
<div class="box">

    <!-- Display an error if there is one -->
    <?php if (isset($_SESSION["error"])): ?>
        <div class="error">
            <?= $_SESSION["error"] ?>
        </div>
        <?php unset($_SESSION["error"]); // Clear error after showing it. ?>
    <?php endif; ?>



    <?php if (!$loggedIn): ?>
        <!-- We are not logged in -->
        <form method="POST">

            <div class="row">
                <div class="one-half column">
                    <label for="username">Username</label>
                    <input class="u-full-width" type="text" name="username" id="username" placeholder="Username">

                </div>
                <div class="one-half column">
                    <label for="password">Password</label>
                    <input class="u-full-width" type="password" name="password" id="password" placeholder="Password">
                </div>
            </div>

            <button type="submit" class="button u-pull-right">Login</button>
        </form>

    <?php else: ?>
        <!-- We are logged in -->
        <h3>Send penger til en annen bruker</h3>


        <form method="get">
            <div class="row">
                <div class="six columns">
                    <label>Bruker</label>
                    <select name="transfer_to" class="u-full-width">
                        <?php foreach ($bankUsers as $bankUser) : ?>
                            <option value="<?= $bankUser["id"] ?>">
                                <?= $bankUser["name"] ?> (kr <?= $bankUser["amount"] ?>,-)
                            </option>
                        <?php endforeach; ?>
                    </select>
                </div>

                <div class="three columns">
                    <label>Beløp</label>
                    <input type="number" name="amount" class="u-full-width" value="10">
                </div>

                <div class="three columns">
                    <label>&nbsp;</label>
                    <button type="submit" class="button u-full-width">Send Penger</button>
                </div>
            </div>

        </form>


        <div class="blue-bar level">
            <div>
                <strong>Beløp tilgjengelig: </strong> kr <?= $_SESSION["user"]["amount"] ?>,-
            </div>

            <div>
                Logget inn som <strong><?= $_SESSION["user"]["name"] ?></strong> -
                <a href="?logout=1" class="logout">Logg ut</a>
            </div>
        </div>

    <?php endif; ?>

</div>


<?php include "../inc/footer.php" ?>

