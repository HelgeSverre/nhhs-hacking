<?php include "../inc/header.php" ?>

<?php

$searchTerm = $_GET["search"] ?? "";

$customers = $db->query("select * from customers where name like '{$searchTerm}%' LIMIT 10")->fetchAll();

?>


<div class="blue-bar">Eksempel på Cross Site Scripting (XSS)</div>
<div class="box" style="margin-bottom: 30px;">

    <form method="get">
        <input type="text" name="search" placeholder="Search customers..." style="width:350px;">
        <button type="submit" class="button">Search</button>
    </form>

    <?php if ($searchTerm): ?>
        <pre>You searched for '<?= $searchTerm ?>' </pre>
    <?php endif; ?>

    <table class="u-full-width">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Country</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($customers as $customer): ?>
            <tr>
                <td><?= $customer["name"] ?></td>
                <td><?= $customer["email"] ?></td>
                <td><?= $customer["country"] ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>


<?php include "../inc/footer.php" ?>

