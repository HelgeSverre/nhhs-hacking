<?php
session_start();

// Fetch configuration
$config = require __DIR__ . "/../config.php";

try {
    // DB Connection used on all pages.
    $db = new PDO("mysql:host={$config['hostname']};dbname={$config['database']}", $config['username'], $config['password']);
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
} catch (PDOException $exception) {

    // The application is not installed
    header("Location: /install");
    die;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>NHHS Hacking Workshop</title>

    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FONT -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600" rel="stylesheet" type="text/css">

    <!-- CSS -->
    <link rel="stylesheet" href="/assets/css/normalize.css">
    <link rel="stylesheet" href="/assets/css/skeleton.css">
    <link rel="stylesheet" href="/assets/css/custom.css">
</head>
<body>


<!-- Header -->
<div class="container" style="margin-top: 40px;">
    <div class="row blue-bar">
        <span class="u-pull-left">NHHS Hacking Workshop</span>
        <span class="u-pull-right">Presentert av <a href="https://helgesverre.no" class="link">Helge Sverre</a></span>
    </div>
    <div class="row">
        <div class="box level">
            <a href="/">
                <img src="/assets/images/nhhs_logo.png" width="180" height="45" class="u-pull-left">
            </a>
            <div class="u-pull-right" style="margin-left: 20px;">
                Praktisk introduksjon til web sikkerhet med eksempler i PHP.
            </div>
        </div>
    </div>
</div>


<!-- Content -->
<div class="container" style="margin-top: 40px;">
    <div class="row">


        <!-- Sidebar Menu -->
        <div class="one-third column" style="margin-bottom: 20px;">
            <div class="blue-bar">
                Eksempler
            </div>
            <div class="box">
                <ul>
                    <li><a href="/pages/sqli.php">SQL Injection</a></li>
                    <li><a href="/pages/reflected-xss.php">Reflected XSS</a></li>
                    <li><a href="/pages/stored-xss.php">Stored XSS</a></li>
                    <li><a href="/pages/auth.php">Broken Authentication</a></li>
                    <li><a href="/pages/lfi.php">Local File Inclusion</a></li>
                    <li><a href="/pages/cmd-inject.php">Command Injection</a></li>
                    <li><a href="/pages/csrf.php">Cross Site Request Forgery</a></li>
                </ul>
            </div>
        </div>


        <!-- Main Content -->
        <div class="two-thirds column">